package com.brainster.finalproject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.security.Key;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.Keys.*;

public class FinalProject {
    private static WebDriver driver;
    private static String email;
    private static String password;
    private static String transporterEmail;

    private static String transporterPassword;

    public static void setup1() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver-107.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public static void clickOn(String elementXpath) {
        WebElement tempElement = driver.findElement(By.xpath(elementXpath));
        tempElement.click();
    }

    public static void typeIn(String elementXpath, String text) {
        driver.findElement(xpath(elementXpath)).sendKeys(text);
    }

    public static void typeInKeys(String elementXpath, Keys text) {
        driver.findElement(xpath(elementXpath)).sendKeys(text);
    }

    public static void editText(String elementXpath, Keys text, String letter, String newText) {
        driver.findElement(xpath(elementXpath)).sendKeys(text, letter, newText);
    }

    public static void selectItemByVisibleText(String elementXpath, String visibleText) {
        Select typeOfUserDdl = new Select(driver.findElement(xpath(elementXpath)));
        typeOfUserDdl.selectByVisibleText(visibleText);
    }

    public static String getText(String elementXpath) {
        return driver.findElement(By.xpath(elementXpath)).getText();
    }

    public static void waitFor(int seconds) {

        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static String navigateTo() {
        driver.get("http://18.156.17.83:9095/");
        return driver.getCurrentUrl();
    }

    public static void end() {
        driver.quit();
    }

    public static String registerMeAsLookingForATransporter() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[1]/button");

        selectItemByVisibleText("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[1]/div[2]/select", "Физичко лице");

        typeIn("//*[@id=\"firstName\"]", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[4]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[6]/div[1]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[6]/div[3]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[6]/div[5]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[6]/div[7]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[6]/div[7]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[6]/div[9]/div[2]/input", "12345");

        email = "email" + System.currentTimeMillis() + "@brainster.co";
        typeIn("//*[@id=\"email\"]", email);

        password = "123456";
        typeIn("//*[@id=\"password\"]", password);

        typeIn("//*[@id=\"confirmPassword\"]", password);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div/div[2]/form/div[15]/input");

        return getText("/html/body/div[3]/div[1]/div/div/h3");
    }

    public static String logMeIn() {
        clickOn("//*[@id=\"login\"]");

        typeIn("//*[@id=\"username\"]", email);

        typeIn("//*[@id=\"password\"]", password);

        clickOn("/html/body/div[1]/div/div/div[2]/div/div[2]/form/button");

        return getText("/html/body/div[3]/div[1]/div[1]/div/div/h3");
    }

    public static String logMeInError(String email, String password) {
        clickOn("//*[@id=\"login\"]");

        typeIn("//*[@id=\"username\"]", email);

        typeIn("//*[@id=\"password\"]", password);

        clickOn("/html/body/div[1]/div/div/div[2]/div/div[2]/form/button");

        waitFor(1);

        return getText("/html/body/div[1]/div/div/div[2]/div/div[1]/div");

    }

    public static void xButton() {
        clickOn("/html/body/div[1]/div/div/div[1]/button");
    }


    public static String sendQuestion() {
        clickOn("/html/body/div[3]/div[1]/div[1]/ul/li[6]/a");

        typeIn("/html/body/div[3]/div[1]/div[2]/div/div/div/div[3]/div[1]/input", "Наслов на прашањето");

        typeIn("/html/body/div[3]/div[1]/div[2]/div/div/div/div[3]/div[2]/textarea", "Прашање");

        clickOn("/html/body/div[3]/div[1]/div[2]/div/div/div/div[3]/button");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div[2]/div/div/div/div[1]/strong");
    }

    public static String editProfile() {
        clickOn("/html/body/div[3]/div[1]/div[1]/ul/li[2]/a/span[2]");

        editText("/html/body/div[3]/div[1]/div[2]/div/div[3]/div/form/div[4]/div/input", CONTROL, "a", "Адреса");

        editText("/html/body/div[3]/div[1]/div[2]/div/div[3]/div/form/div[5]/div/input", CONTROL, "a", "Град");

        clickOn("/html/body/div[3]/div[1]/div[2]/div/div[3]/div/form/div[11]/div/div/button[1]");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div[2]/div/div[1]/strong");
    }

    public static String sendRequest() {
        clickOn("/html/body/div[3]/div[1]/div[1]/ul/li[3]/a/span[2]");

        typeIn("//*[@id=\"newRequestForm\"]/div/div[2]/div[2]/input", "Скопје-Париз селидба5");

        selectItemByVisibleText("//*[@id=\"field_y\"]", "Селидбени работи");

        typeIn("//*[@id=\"newRequestForm\"]/div/div[4]/div[2]/place-search-field/input", "Скопје");
        waitFor(2);
        typeInKeys("//*[@id=\"newRequestForm\"]/div/div[4]/div[2]/place-search-field/input", ARROW_DOWN);
        typeInKeys("//*[@id=\"newRequestForm\"]/div/div[4]/div[2]/place-search-field/input", ENTER);

        typeIn("//*[@id=\"newRequestForm\"]/div/div[5]/div[2]/place-search-field/input", "Париз");
        waitFor(2);
        typeInKeys("//*[@id=\"newRequestForm\"]/div/div[5]/div[2]/place-search-field/input", ARROW_DOWN);
        typeInKeys("//*[@id=\"newRequestForm\"]/div/div[5]/div[2]/place-search-field/input", ENTER);

        typeIn("//*[@id=\"newRequestForm\"]/div/div[10]/div/div[2]/textarea", "Опис");

        clickOn("//*[@id=\"cachePickup\"]");

        clickOn("//*[@id=\"newRequestForm\"]/div/div[14]/input");

        return getText("/html/body/div[3]/div[1]/div[2]/div/request-list-pagination/jhi-alert/div/div/div/div/pre");
    }

    public static String logMeOut() {
        clickOn("//*[@id=\"logout2\"]");
        waitFor(2);
        return driver.getCurrentUrl();
    }

    public static String registerMeAsATransporter() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/h3");
    }

    public static String logMeInAsTransporter() {
        clickOn("//*[@id=\"login\"]");

        typeIn("//*[@id=\"username\"]", transporterEmail);

        typeIn("//*[@id=\"password\"]", transporterPassword);

        clickOn("/html/body/div[1]/div/div/div[2]/div/div[2]/form/button");

        return getText("/html/body/div[3]/div[1]/div[1]/div/div/h3");
    }

    public static String findRequestSendOffer() {
        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[1]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[1]/country-selector/div/input[1]", "Macedonia" + ENTER);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[2]/country-selector/div/input[1]", "France" + ENTER);

        selectItemByVisibleText("//*[@id=\"field_y\"]", "Селидбени работи");

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[3]/a/span[2]");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[2]/request-list/div[2]/div[2]/table/tbody/tr[1]/td[1]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div[1]/div[5]/div/div[2]/div/button");

        waitFor(2);

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[2]/div[2]/table/tbody/tr/td[5]/input", "100");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[1]/input", "03.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[2]/input", "04.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[3]/input", "05.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[2]/div/textarea", "Порака до клиентот");

        clickOn("/html/body/div[3]/div[1]/div[2]/form/div/div[5]/button");

        clickOn("/html/body/div[1]/div/div/div[3]/button[1]");

        waitFor(2);

        return getText("/html/body/div[3]/div[1]/div[2]/div[1]/div[5]/div/div[2]/div");
    }

    public static String findRequestSendASecondOffer() {
        clickOn("/html/body/div[1]/nav/div[1]/a/img");

        // waitFor(5);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[1]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[1]/country-selector/div/input[1]", "Macedonia" + ENTER);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[2]/country-selector/div/input[1]", "France" + ENTER);

        selectItemByVisibleText("//*[@id=\"field_y\"]", "Селидбени работи");

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[3]/a/span[2]");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[2]/request-list/div[2]/div[2]/table/tbody/tr[2]/td[1]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div[1]/div[5]/div/div[2]/div/button");

        waitFor(2);

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[2]/div[2]/table/tbody/tr/td[5]/input", "100");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[1]/input", "03.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[2]/input", "04.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[3]/input", "05.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[2]/div/textarea", "Порака до клиентот");

        clickOn("/html/body/div[3]/div[1]/div[2]/form/div/div[5]/button");

        clickOn("/html/body/div[1]/div/div/div[3]/button[1]");

        waitFor(2);

        return getText("/html/body/div[3]/div[1]/div[2]/div[1]/div[5]/div/div[2]/div");
    }

    public static String findRequestSendAThirdOffer() {
        clickOn("/html/body/div[1]/nav/div[1]/a/img");

        waitFor(5);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[1]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[1]/country-selector/div/input[1]", "Macedonia" + ENTER);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[2]/div[1]/div/div[2]/country-selector/div/input[1]", "France" + ENTER);

        selectItemByVisibleText("//*[@id=\"field_y\"]", "Селидбени работи");

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[1]/div[2]/request-search/div/div/div[3]/a/span[2]");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div[2]/request-search-list/div[2]/request-list/div[2]/div[2]/table/tbody/tr[3]/td[1]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div[1]/div[5]/div/div[2]/div/button");

        waitFor(2);

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[2]/div[2]/table/tbody/tr/td[5]/input", "100");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[1]/input", "03.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[2]/input", "04.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[1]/div[3]/input", "05.12.2022 00:00");

        typeIn("/html/body/div[3]/div[1]/div[2]/form/div/div[4]/div/div[2]/div/textarea", "Порака до клиентот");

        clickOn("/html/body/div[3]/div[1]/div[2]/form/div/div[5]/button");

        clickOn("/html/body/div[1]/div/div/div[3]/button[1]");

        waitFor(2);

        return getText("/html/body/div[3]/div[1]/div[2]/div[1]/div[5]/div/div[2]/div");
    }

    public static String acceptOffer() {
        clickOn("/html/body/div[3]/div[1]/div[1]/ul/li[4]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div/request-list-pagination/request-list/div[2]/div[2]/table/tbody/tr[1]/td[1]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div[1]/div[6]/div[2]/div/div/div[2]/div/div[8]/a");

        clickOn("//*[@id=\"offer0\"]");

        clickOn("//*[@id=\"offersSet5290\"]/div/div[1]/div[2]/div[6]/input");

        return getText("/html/body/div[3]/div[1]/div[2]/h2");
    }

    public static String refuseOffer() {
        clickOn("/html/body/div[3]/div[1]/div[1]/ul/li[4]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div/request-list-pagination/request-list/div[2]/div[2]/table/tbody/tr[1]/td[1]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div[1]/div[6]/div[2]/div/div/div[2]/div/div[8]/a");

        clickOn("/html/body/div[3]/div[1]/div[2]/div[2]/button");

        return getText("/html/body/div[3]/div[1]/div[2]/ul/li[3]/a/span[2]");
    }

    public static String contactForm() {
        clickOn("/html/body/div[3]/div[2]/div[1]/ul/li[3]/a");

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[3]/div[2]/input", "Ime");

        email = "email" + System.currentTimeMillis() + "@live.com";
        typeIn("/html/body/div[3]/div[1]/div/div/div/div[4]/div[2]/input", email);

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[5]/div[2]/input", "Naslov");

        typeIn("/html/body/div[3]/div[1]/div/div/div/div[6]/div[2]/textarea", "Poraka");

        clickOn("/html/body/div[3]/div[1]/div/div/div/div[7]/button");

        waitFor(2);

        return getText("/html/body/div[3]/div[1]/div/div/div/div[1]/div/div[1]/strong");
    }

    public static String registerMeAsATransporterEmpty() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/div/p");
    }

    public static String registerMeAsATransporterEmailError(String transporterEmail) {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[3]/div/p[2]");
    }

    public static String registerMeAsATransporterPasswordError(String transporterPassword) {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[5]/div/p[2]");
    }

    public static String registerMeAsATransporterConfirmationPasswordError(String confirmationTransporterPassword) {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", confirmationTransporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[7]/div/p[3]");
    }

    public static String registerMeAsATransporterPasswordNotMatched(String transporterPassword, String transporterPasswordConf) {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPasswordConf);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[1]/div[3]");
    }

    public static String registerMeAsATransporterWithoutFirstName() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[2]/div/p");
    }

    public static String registerMeAsATransporterWithoutLastName() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[4]/div/p");
    }

    public static String registerMeAsATransporterWithoutFirmName() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[6]/div/p");
    }

    public static String registerMeAsATransporterWithoutAddress() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[8]/div/p");
    }

    public static String registerMeAsATransporterWithoutCity() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[10]/div/p");
    }

    public static String registerMeAsATransporterWithoutFax() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[12]/div/p");
    }

    public static String registerMeAsATransporterWithoutCountry() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[14]/div/p");
    }

    public static String registerMeAsATransporterWithoutTelephone() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("//*[@id=\"acceptTerms\"]");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[17]/div/p");
    }

    public static String registerMeAsATransporterWithoutTermsAndConditions() {
        clickOn("/html/body/div[1]/nav/div[3]/ul/li[3]/a");

        waitFor(2);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/button");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[1]/div[2]/input", "FirstName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[3]/div[2]/input", "LastName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[5]/div[2]/input", "FirmName");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[7]/div[2]/input", "Address");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[9]/div[2]/input", "City");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[11]/div[2]/input", "ZipCode");

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/div[1]/span");
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[13]/div[2]/country-selector/div/input[1]", "Macedonia" + ENTER);

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[15]/div[2]/input", "12345");

        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[1]/div[16]/div[2]/input", "12345");

        transporterEmail = "transporteremail" + System.currentTimeMillis() + "@brainster.co";
        typeIn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[2]/div[2]/input", transporterEmail);

        transporterPassword = "123456";
        typeIn("//*[@id=\"password\"]", transporterPassword);

        typeIn("//*[@id=\"confirmPassword\"]", transporterPassword);

        clickOn("/html/body/div[3]/div[1]/div/div/div[2]/form/div[10]/input");

        waitFor(1);

        return getText("/html/body/div[3]/div[1]/div/div/div[2]/form/div[9]/div/p");
    }

}

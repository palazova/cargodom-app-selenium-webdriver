package com.brainster.finalproject;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class FinalProjectTest {
    @BeforeClass
    public void beforeTest() {
        FinalProject.setup1();
    }

    @Test(priority = 1)
    public void testForVerifyingUserReachedHomePage() {
        Assert.assertEquals(FinalProject.navigateTo(), "http://18.156.17.83:9095/");
    }

    @Test(priority = 2, enabled = true)
    public void testForVerifyingUserIsRegistered() {
        Assert.assertEquals(FinalProject.registerMeAsLookingForATransporter(), "Вашиот профил е успешно креиран! Ве молиме проверете го Вашиот e-mail за да ја завршите регистрацијата.");
    }

    @Test(priority = 3, enabled = true)
    public void testLogInErrorInvalidPassword() {
        Assert.assertEquals(FinalProject.logMeInError("emailnew@live.com", "Password123"), "Вашиот обид е неуспешен! Ве молиме проверете го вашето корисничко име и лозинка и обидете се повторно.");
    }

    @Test(priority = 4, enabled = true)
    public void testXButton() {
        FinalProject.xButton();
    }

    @Test(priority = 5, enabled = true)
    public void testLogInErrorInvalidEmail() {
        Assert.assertEquals(FinalProject.logMeInError("emailnew@live.", "Password"), "Вашиот обид е неуспешен! Ве молиме проверете го вашето корисничко име и лозинка и обидете се повторно.");
    }

    @Test(priority = 6, enabled = true)
    public void testXButton0() {
        FinalProject.xButton();
    }

    @Test(priority = 7, enabled = true)
    public void testLogInErrorNoPassword() {
        Assert.assertEquals(FinalProject.logMeInError("emailnew@live.com", ""), "Вашиот обид е неуспешен! Ве молиме проверете го вашето корисничко име и лозинка и обидете се повторно.");
    }

    @Test(priority = 8, enabled = true)
    public void testXButton1() {
        FinalProject.xButton();
    }

    @Test(priority = 9, enabled = true)
    public void testLogInErrorNoEmail() {
        Assert.assertEquals(FinalProject.logMeInError("", "Password"), "Вашиот обид е неуспешен! Ве молиме проверете го вашето корисничко име и лозинка и обидете се повторно.");
    }

    @Test(priority = 10, enabled = true)
    public void testXButton2() {
        FinalProject.xButton();
    }

    @Test(priority = 11, enabled = true)
    public void testLogInErrorNoEmailAndPassword() {
        Assert.assertEquals(FinalProject.logMeInError("", ""), "Вашиот обид е неуспешен! Ве молиме проверете го вашето корисничко име и лозинка и обидете се повторно.");
    }

    @Test(priority = 12, enabled = true)
    public void testXButton3() {
        FinalProject.xButton();
    }

    @Test(priority = 13, enabled = true)
    public void testLogMeInWithNewlyCreatedUser() {
        Assert.assertEquals(FinalProject.logMeIn(), "FirstName LastName");
    }

    @Test(priority = 14, enabled = true)
    public void testSendQuestion() {
        Assert.assertEquals(FinalProject.sendQuestion(), "Вашиот меил е успешно испратен до одделот за корисничка поддршка!");
    }

    @Test(priority = 15, enabled = true)
    public void testEditProfile() {
        Assert.assertEquals(FinalProject.editProfile(), "Успешно ажурирање!");
    }

    @Test(priority = 16, enabled = true)
    public void testNewRequest() {
        Assert.assertEquals(FinalProject.sendRequest(), "Успешно е креирано ново барање");
    }

    @Test(priority = 17, enabled = true)
    public void testNewRequest1() {
        Assert.assertEquals(FinalProject.sendRequest(), "Успешно е креирано ново барање");
    }

    @Test(priority = 18, enabled = true)
    public void testNewRequest2() {
        Assert.assertEquals(FinalProject.sendRequest(), "Успешно е креирано ново барање");
    }

    @Test(priority = 19, enabled = true)
    public void testLogMeOut() {
        Assert.assertEquals(FinalProject.logMeOut(), "http://18.156.17.83:9095/");
    }

    @Test(priority = 20, enabled = true)
    public void testLogInAsTransporterErrorEmptyFields() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterEmpty(), "Ве молиме пополнете ги сите задолжителни полиња!");
    }

    @Test(priority = 21, enabled = true)
    public void testLogInAsTransporterErrorEmailWithoutPrefix() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterEmailError("@live.com"), "Вашиот e-mail не е валиден.");
    }

    @Test(priority = 22, enabled = true)
    public void testLogInAsTransporterErrorEmailWithoutAtSign() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterEmailError("livelive.com"), "Вашиот e-mail не е валиден.");
    }

    @Test(priority = 23, enabled = true)
    public void testLogInAsTransporterErrorEmailWithoutSuffix() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterEmailError("live@"), "Вашиот e-mail не е валиден.");
    }

    @Test(priority = 24, enabled = true)
    public void testLogInAsTransporterErrorEmailMin5Characters() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterEmailError("abcd"), "Вашиот e-mail не е валиден.");
    }

    @Test(priority = 25, enabled = true)
    public void testLogInAsTransporterErrorPasswordMin4Characters() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterPasswordError("abc"), "Вашата лозинка треба да биде од најмалку 4 карактери.");
    }

    @Test(priority = 26, enabled = true)
    public void testLogInAsTransporterErrorPasswordMax50Characters() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterPasswordError("111111111111111111111111111111111111111111111111111"), "Вашата лозинка неможе да биде подолга од 50 карактери.");
    }

    @Test(priority = 27, enabled = true)
    public void testLogInAsTransporterErrorConfirmationPasswordMin4Characters() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterConfirmationPasswordError("abc"), "Потврдта на лозинката треба да биде од најмалку 4 карактери.");
    }

    @Test(priority = 28, enabled = true)
    public void testLogInAsTransporterErrorConfirmationPasswordMax50Characters() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterConfirmationPasswordError("111111111111111111111111111111111111111111111111111"), "Потврдата на лозинката неможе да биде подолга од 50 карактери.");
    }

    @Test(priority = 29, enabled = true)
    public void testLogInAsTransporterErrorPasswordNotMatched() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterPasswordNotMatched("abcd", "abcdw"), "Вашата лозинка не соодветсвува со потврдената лозинка!");
    }

    @Test(priority = 30, enabled = true)
    public void testRegisterMeAsATransporterWithoutFirstName() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutFirstName(), "Ова поле е задолжително.");
    }

    @Test(priority = 31, enabled = true)
    public void testRegisterMeAsATransporterWithoutLastName() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutLastName(), "Ова поле е задолжително.");
    }

    @Test(priority = 32, enabled = true)
    public void testRegisterMeAsATransporterWithoutCompanyName() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutFirmName(), "Ова поле е задолжително.");
    }

    @Test(priority = 33, enabled = true)
    public void testRegisterMeAsATransporterWithoutAddress() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutAddress(), "Ова поле е задолжително.");
    }

    @Test(priority = 34, enabled = true)
    public void testRegisterMeAsATransporterWithoutCity() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutCity(), "Ова поле е задолжително.");
    }

    @Test(priority = 35, enabled = true)
    public void testRegisterMeAsATransporterWithoutPost() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutFax(), "Ова поле е задолжително.");
    }

    @Test(priority = 36, enabled = true)
    public void testRegisterMeAsATransporterWithoutCountry() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutCountry(), "Ова поле е задолжително.");
    }

    @Test(priority = 37, enabled = true)
    public void testRegisterMeAsATransporterWithoutTerms() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutTermsAndConditions(), "Ова поле е задолжително.");
    }

    @Test(priority = 38, enabled = true)
    public void testRegisterMeAsATransporterWithoutTelephone() {
        Assert.assertEquals(FinalProject.registerMeAsATransporterWithoutTelephone(), "Ова поле е задолжително.");
    }

    @Test(priority = 39, enabled = true)
    public void testRegisterMeAsATransporter() {
        Assert.assertEquals(FinalProject.registerMeAsATransporter(), "Вашиот профил е успешно креиран! Ве молиме проверете го Вашиот e-mail за да ја завршите регистрацијата.");
    }

    @Test(priority = 40, enabled = true)
    public void testLogInAsTransporter() {
        Assert.assertEquals(FinalProject.logMeInAsTransporter(), "FirstName LastName");
    }

    @Test(priority = 41, enabled = true)
    public void testFindRequestSendOffer() {
        Assert.assertEquals(FinalProject.findRequestSendOffer(), "ИЗМЕНИ ЈА ПОНУДАТА ОТКАЖИ ЈА ПОНУДАТА");
    }

    @Test(priority = 42, enabled = true)
    public void testFindRequestSendOffer2() {
        Assert.assertEquals(FinalProject.findRequestSendASecondOffer(), "ИЗМЕНИ ЈА ПОНУДАТА ОТКАЖИ ЈА ПОНУДАТА");
    }

    @Test(priority = 43, enabled = true)
    public void testFindRequestSendOffer3() {
        Assert.assertEquals(FinalProject.findRequestSendAThirdOffer(), "ИЗМЕНИ ЈА ПОНУДАТА ОТКАЖИ ЈА ПОНУДАТА");
    }

    @Test(priority = 44, enabled = true)
    public void testLogMeOutFromTransporter() {
        Assert.assertEquals(FinalProject.logMeOut(), "http://18.156.17.83:9095/");
    }

    @Test(priority = 45, enabled = true)
    public void testLogMeInAsAnEndUser() {
        Assert.assertEquals(FinalProject.logMeIn(), "FirstName LastName");
    }

    @Test(priority = 46, enabled = true)
    public void testForAcceptedOffer() {
        Assert.assertEquals(FinalProject.acceptOffer(), "Прифатени понуди за транспорт");
    }

    @Test(priority = 47, enabled = true)
    public void testForAcceptedOffer2() {
        Assert.assertEquals(FinalProject.acceptOffer(), "Прифатени понуди за транспорт");
    }

    @Test(priority = 48, enabled = true)
    public void testForRefusedOffer() {
        Assert.assertEquals(FinalProject.refuseOffer(), "ОТКАЖАНО");
    }

    @Test(priority = 49, enabled = true)
    public void testFinalLogOut() {
        Assert.assertEquals(FinalProject.logMeOut(), "http://18.156.17.83:9095/");
    }

    @Test(priority = 50, enabled = true)
    public void tesContactForm() {
        Assert.assertEquals(FinalProject.contactForm(), "Вашиот меил е успешно испратен до одделот за корисничка поддршка!");
    }

    @AfterClass
    public void afterTest() {
        FinalProject.end();
    }

}


